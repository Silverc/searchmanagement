Running Docker:

1.Run the "docker pull postgres" command to pull the postgres image

2.
Run the following command to set up the postgres container
 "docker run --name postgres -e POSTGRES_PASSWORD=Thunder500 -e POSTGRES_USER=postgres -e POSTGRES_DB=sale-service -d -p 5432:5432 postgres"

3.
Run the following command to link the system image to the postgres one
"docker run --link postgres carlosmanuelfigueiredo/salesearch_manager:1.0"
