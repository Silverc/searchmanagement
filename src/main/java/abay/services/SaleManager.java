package abay.services;

import abay.entities.Sale;
import abay.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SaleManager {
    //private List<Sale> sales = new ArrayList<Sale>();

    final SaleRepository saleRepository;
    AtomicInteger id_counter=new AtomicInteger(0);

    @Autowired
    public SaleManager(SaleRepository saleRepository){
        this.saleRepository=saleRepository;
    }

    public List<Sale> getSales() {
        List<Sale> list = new ArrayList<>();
        saleRepository.findAll().forEach(e -> list.add(e));
        return list;
    }

    public boolean addSale(Sale newSale) {
        //sales.add(newSale);
        newSale.setId(id_counter);
        id_counter.incrementAndGet();
        saleRepository.save(newSale);

        return true;
    }

    public boolean cancelSale(AtomicInteger id) {
        Sale sale=saleRepository.findByid(id);
        if(sale!=null){
            sale.setStatus("cancelled");

            //this will update the existing entry right?
            saleRepository.save(sale);

            return true;
        }

        return false;

        /*
        //Before repositories...
        for (Sale sale : sales) {
            if (sale.getId().equals(id)) {
                sale.setStatus("cancelled");
                return true;
            }
        }
        return false;*/
    }

    public boolean modifySale(AtomicInteger id, List<String> productCategories, String productDescription, Integer productQuantity, Date limitDate, BigDecimal unitaryPrice,String productCondition){
        boolean ret=false;
        Sale sale=saleRepository.findByid(id);
        if(productCategories!=null){
            sale.setProductCategories(productCategories);
            ret= true;
        }
        if(productDescription!=null){
            sale.setProductDescription(productDescription);
            ret= true;
        }
        if(productQuantity!=null){
            sale.setProductQuantity(productQuantity);
            ret=true;
        }
        if(limitDate!=null){
            sale.setLimitDate(limitDate);
            ret=true;
        }
        if(unitaryPrice!=null){
            sale.setUnitaryPrice(unitaryPrice);
            ret=true;
        }
        if(productCondition!=null){
            sale.setProductCondition(productCondition);
            ret=true;
        }
        if(ret){
            saleRepository.save(sale);
        }
        return ret;
        /*//before repositories
        boolean ret=false;

        for (Sale sale : sales) {
            if (sale.getId().equals(id)) {
                if(productCategories!=null){
                    sale.setProductCategories(productCategories);
                    ret= true;
                }
                if(productDescription!=null){
                    sale.setProductDescription(productDescription);
                    ret= true;
                }
                if(productQuantity!=null){
                    sale.setProductQuantity(productQuantity);
                    ret=true;
                }
                if(limitDate!=null){
                    sale.setLimitDate(limitDate);
                    ret=true;
                }
                if(unitaryPrice!=null){
                    sale.setUnitaryPrice(unitaryPrice);
                    ret=true;
                }
                if(productCondition!=null){
                    sale.setProductCondition(productCondition);
                    ret=true;
                }
            }
        }
        return ret;*/
    }
}
