package abay.Requests;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toshiba on 22/06/2020.
 */

public class SearchSaleRequest {

    private List<String> category;
    private String status;
    private String type;
    private String name;


    public SearchSaleRequest(){
    }
    public SearchSaleRequest(JsonObject obj){
        JsonArray jsonCategories = (JsonArray) obj.get("category");
        this.category = new ArrayList<>();

        //to remove the categories from the JSON file, we have to
        // search it and pass them one by one to categories
        for(int i=0;i<jsonCategories.size();i++)
        {
            this.category.add(jsonCategories.get(i).toString());
        }

        this.status = obj.get("status").toString();
        this.type = obj.get("type").toString();
    }

    public List<String> getCategory() {
        return category;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){this.name=name;
    }

    public void setType(String type){this.type=type;
    }

    public void setCategory(List<String> category){this.category=category;
    }

    public void setStatus(String status) {
        this.status=status;
    }
    }
