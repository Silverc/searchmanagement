package abay.restservice;

import abay.Requests.CancelSaleRequest;
import abay.Requests.SearchSaleRequest;
import abay.repositories.SaleRepository;
import abay.responses.CancelSaleResponse;
import abay.Requests.NewSaleRequest;
import abay.responses.NewSaleResponse;
import abay.responses.SearchSaleResponse;
import abay.controllers.SearchController;
import org.json.simple.JSONArray;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@ComponentScan(basePackages = { "abay"} )
@EntityScan("abay.entities")
@EnableJpaRepositories("abay.repositories")
public class RestServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestServiceApplication.class, args);


        // TESTS
        //new Sale test
        /*String url = "http://localhost:8080/newSale";
        NewSaleRequest test=new NewSaleRequest();
        test.setSeller("fsf1");
        test.setType("fdfs");
        test.setUnitaryPrice(new BigDecimal(1));
        test.setProductQuantity(2);
        test.setProductDescription("fsafsfa");
        List<String> categories= Arrays.asList("foo", "bar");
        test.setProductCategories(categories);

        RestTemplate template = new RestTemplate();
        NewSaleResponse result = template.postForObject(url, test,NewSaleResponse.class);
        System.out.println(result.getSuccess());*/

        //Tests the /searchAll part of the program
        JsonObject obj = new JsonObject();

        //creates an array for the categories
        JsonArray arr = new JsonArray();
        //arr.add("foo");
        //arr.add("dad");

        obj.put("category", arr);
        obj.put("status", "");
        obj.put("type", "");
        String url = "http://localhost:8080/searchSales";

        SaleRepository rep;

        SearchSaleRequest test=new SearchSaleRequest(obj);

        RestTemplate template = new RestTemplate();
        SearchSaleResponse result = template.postForObject(url, test,SearchSaleResponse.class);
        //SearchSaleResponse result2 = template.postForObject(url, test, SearchSaleResponse.class, obj);
        System.out.println(result.getSuccess());


        //Cancel Sale test
        /*String url2 = "http://localhost:8080/cancelSale";

        CancelSaleRequest test2=new CancelSaleRequest();
        test2.setId(new AtomicInteger(1));//


        RestTemplate template2 = new RestTemplate();
        CancelSaleResponse result2 = template2.postForObject(url2, test2,CancelSaleResponse.class);
        System.out.println(result2.getSuccess());

        //Cancel Sale test, should say False

        test2.setId(new AtomicInteger(2));
        CancelSaleResponse result3 = template2.postForObject(url2, test2,CancelSaleResponse.class);
        System.out.println(result3.getSuccess());*/


    }

}
