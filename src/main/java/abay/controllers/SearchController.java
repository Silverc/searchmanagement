package abay.controllers;

import abay.Requests.SearchSaleRequest;
import abay.entities.Sale;
import abay.responses.SearchSaleResponse;
import abay.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.json.simple.JsonObject;

import java.util.List;

@Controller
public class SearchController {

    final SearchService searchManager;

    @Autowired
    public SearchController(SearchService searchManager) {
        this.searchManager = searchManager;
    }

    @PostMapping(value = "/searchSales")
    @ResponseBody
    public SearchSaleResponse searchAllSales(@RequestBody SearchSaleRequest searchSaleRequest) {

        boolean success = false;

        List<Sale> query_result = searchManager.getSales();

        //in order to search the sales with the data in the JSON file
        // (and allowing for empty fields)we search all sales and check
        // if they have the parameters we have searched for
        for (int i = 0; i < query_result.size(); i++) {
                System.out.println(query_result.get(i).getProductDescription());

                //checks if the products found have the same categories, name, type and status as searched
                if(query_result.get(i).getProductCategories().containsAll(searchSaleRequest.getCategory()) && query_result.get(i).getType().contains(searchSaleRequest.getType()) && query_result.get(i).getStatus().contains(searchSaleRequest.getStatus()) && query_result.get(i).getProductDescription().contains(searchSaleRequest.getName()))
                {
                    success = true;
                }
            }

        return new SearchSaleResponse(success);
    }


}
