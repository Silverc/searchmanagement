package abay.responses;

public class ModifySaleResponse {
    private boolean success;

    public ModifySaleResponse() {
    }

    public ModifySaleResponse(boolean success) {
        this.success = success;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
