package abay.responses;

public class CancelSaleResponse {
    private boolean success;

    public CancelSaleResponse() {
    }

    public CancelSaleResponse(boolean success) {
        this.success = success;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
