
/**
 * Created by toshiba on 22/06/2020.
 */
package abay.responses;

public class SearchSaleResponse {
    private boolean success;

    public SearchSaleResponse() {
    }

    public SearchSaleResponse(boolean success) {
        this.success = success;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
